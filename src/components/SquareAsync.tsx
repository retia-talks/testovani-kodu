import { useEffect, useState } from "react";

export const SquareAsync = ({ id, color }: { id: number; color: string }) => {
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState<any>(null);

  useEffect(() => {
    async function fetchData() {
      // You can await here
      const response = await fetch(
        "https://www.thecolorapi.com/id?rgb=0,71,17&format=json"
      );

      return response.json();
    }

    fetchData().then((data) => {
      setData(data);
      setLoading(false);
    });
  }, []);

  return (
    <>
      {loading ? (
        <div>loading...</div>
      ) : (
        <div
          style={{
            width: 50,
            height: 50,
            display: "inline-block",
            float: "left",
            backgroundColor: color,
          }}
          data-test="square"
          data-testid={data?.name?.value}
        >
          <span style={{ position: "relative", top: "calc(50% - .5rem)" }}>
            {`${id + 1}-${data?.name?.value}`}
          </span>
        </div>
      )}
    </>
  );
};
