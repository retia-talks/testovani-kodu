import React from "react";
import { render } from "@testing-library/react";

import { Content } from "./Content";

// FYI: pozor na getRandomColor() > vraci jiny vysledek pri kazdem volani
jest.mock("../functions", () => ({
  getRandomColor: () => `rgb(0, 0, 0)`,
  //  getRandomColor: jest.fn(() => `rgb(0, 0, 0)`),
}));

jest.mock("./Layout", () => ({
  CenterPanel: ({ children }: { children: React.ReactNode }) => (
    <div>{children}</div>
  ),
  Square: ({ id, color }: { id: number; color: string }) => (
    <div data-test={id}>
      {id}-{color}
    </div>
  ),
}));

describe("Content", () => {
  test("should render 3 items", () => {
    const { container } = render(<Content clicks={3} />);
    expect(container).toMatchSnapshot();
  });

  test("should render 0 items", () => {
    const { container } = render(<Content clicks={0} />);
    expect(container).toMatchSnapshot();
  });
});
