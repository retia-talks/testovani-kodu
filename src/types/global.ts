interface GlobalState {
  data: AppState;
}

interface AppState {
  counter: number;
  recording: boolean;
}

interface AppAction {
  type: string;
  iterator?: number;
  enabled?: boolean;
}

export type { GlobalState, AppAction, AppState };