import { fireEvent, render, screen } from "@testing-library/react";

import { Square } from "./Layout";

const color = "rgb(0, 0, 0)";

describe("Square", () => {
  test("should render item", () => {
    render(<Square id={1} color={color} />);

    const square = screen.getByTestId(1);

    expect(square).toHaveStyle(`background-color: ${color}`);
    expect(square).toHaveTextContent("");

    fireEvent.click(square);

    expect(square).toHaveTextContent("2");
  });

  test("should match snapshot", () => {
    const { container } = render(<Square id={1} color={color} />);
    expect(container).toMatchSnapshot();
  });
});
