import { getRandomColor } from "./functions";

describe("getRandomColor", () => {
  test("should return a string", () => {
    expect(typeof getRandomColor()).toBe("string");
  });

  test("should return a string in the format rgb(x, x, x)", () => {
    expect(getRandomColor()).toMatch(
      /^rgb\((\d{1,3}), (\d{1,3}), (\d{1,3})\)$/
    );
  });
});
