import { Content } from "./Content";

//docs.cypress.io/guides/component-testing/overview

const squareSelector = '[data-test="square"]';

describe("<Content />", () => {
  it("renders", () => {
    // see: https://on.cypress.io/mounting-react
    cy.mount(<Content clicks={3} />);

    cy.get(squareSelector).should("have.attr", "style");
    cy.get(squareSelector).should("have.length", 3);
    cy.get(squareSelector).should("have.css", "width", "50px");
  });
});
